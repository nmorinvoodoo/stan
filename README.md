## Sentia demo app
This was an Android native demo app for Sentia, made in Kotlin.
They asked to display the listing of the properties described here: https://demo6672118.mockable.io/listings (the detail fragment/activity had no importance as long as you could see that it was updated when you clicked on different)

## Project structure
The project has two modules
- ws-core who handles all network related tasks
- app which handles the rest.

App depends on ws-core, not the opposite.

## Libraries used
- [Retrofit](https://square.github.io/retrofit/) for the requests, with [Moshi](https://github.com/square/moshi) to parse the Json responses.
- [RxJava](https://github.com/ReactiveX/RxJava) and RxAndroid to handle asynchronous tasks.
- Android Architecture Components ViewModel and LiveData to make those tasks lifecycle-aware
- [Picasso](http://square.github.io/picasso/) to help with the images
- And of course the Android Support Library



## TODO
A lot of things are yet to be done.

Things I really wish I did before I sent you the code:
- Tests, tests, tests.
- Api Mocks

Things that would have done after, had I more time:
- A viewPager for the premium listings pictures
- get rid of the last recyclerview decorator
- find a fitting animation (I did one on the listing image which went to the detail's activity toolbar, but it was rather ugly)
