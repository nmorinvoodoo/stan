package com.example.nicolasm.sentiademo

import android.app.Application
import com.example.nicolasm.sentiademo.di.AppComponent
import com.example.nicolasm.sentiademo.di.DaggerAppComponent
import com.example.wscore.di.NetworkModule

class SentiaApp: Application() {

    var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        appComponent = initDagger()
    }

    private fun initDagger(): AppComponent? {
        return DaggerAppComponent.builder()
                .networkModule(NetworkModule())
                .build()
    }
}