package com.example.nicolasm.sentiademo

/**
 * Returns `true` if this char sequence contains one of the specified [others] sequences of characters as a substring.
 *
 * @param ignoreCase `true` to ignore character case when comparing strings. By default `false`.
 */
fun CharSequence.endsWithAny(vararg others: String, ignoreCase: Boolean = false): Boolean = others.any { endsWith(it, ignoreCase) }