package com.example.nicolasm.sentiademo.ui.propertydetails

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.nicolasm.sentiademo.R
import com.example.nicolasm.sentiademo.model.PropertyObject
import kotlinx.android.synthetic.main.activity_property_detail.*
import kotlinx.android.synthetic.main.property_detail.view.*

/**
 * A fragment representing a single property detail screen.
 * This fragment is either contained in a [com.example.nicolasm.sentiademo.ui.propertylist.PropertyListActivity]
 * in two-pane mode (on tablets) or a [PropertyDetailsActivity]
 * on handsets.
 */
class PropertyDetailsFragment : Fragment(), IPropertyContainer {
    private lateinit var property: PropertyObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_PROPERTY)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                property = it.getParcelable(ARG_PROPERTY)
                activity?.toolbar_layout?.title = property.title
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.property_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshDisplay()
    }

    /**
     * As indicated in the directions, the fragment is almost empty.
     * We display the number of car because the description is always the same. With the # of car spots,
     * we can see that the content of the fragment is updated when the user clicks on a new property
     */
    private fun refreshDisplay() {
        property.let {
            view?.propertyDetailDescriptionTV?.text = """${it.description}${it.carSpots}"""
        }
    }

    /**
     * Called directly from the activity when a user clicks on a property, when a [PropertyDetailsFragment]
     * already exists and is displayed
     */
    override fun updateProperty(property: PropertyObject) {
        this.property = property
        refreshDisplay()
    }

    companion object {
        const val ARG_PROPERTY = "arg_property"

        const val TAG = "PropertyDetailsFragment"
    }

}

interface IPropertyContainer {

    fun updateProperty(property: PropertyObject)
}
