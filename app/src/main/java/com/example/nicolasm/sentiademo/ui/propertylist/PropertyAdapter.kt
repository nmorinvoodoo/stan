package com.example.nicolasm.sentiademo.ui.propertylist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.nicolasm.sentiademo.R
import com.example.nicolasm.sentiademo.model.PropertyObject

class PropertyAdapter(private val listener: OnPropertyClickListener) : RecyclerView.Adapter<AbstractPropertyViewHolder>() {

    private val properties: ArrayList<PropertyObject> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractPropertyViewHolder {
        // we use the viewType as the layout id, so we can create the view directly
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        val viewHolder: AbstractPropertyViewHolder = if (viewType == VIEW_HOLDER_TYPE_PREMIUM)
            PremiumPropertyViewHolder(view)
        else
            PropertyViewHolder(view)
        viewHolder.listener = listener
        return viewHolder
    }

    override fun getItemCount(): Int {
        return properties.size
    }

    override fun onBindViewHolder(viewHolder: AbstractPropertyViewHolder, position: Int) {
        viewHolder.bind(properties[position])
    }

    override fun getItemViewType(position: Int): Int {
        return if (properties[position].isPremium)
            VIEW_HOLDER_TYPE_PREMIUM
        else
            VIEW_HOLDER_TYPE_REGULAR
    }

    fun updateContent(props: List<PropertyObject>) {
        if (props.isNotEmpty()) {
            properties.clear()
            properties.addAll(props)
            notifyDataSetChanged()
        }
    }

    companion object {
        private const val VIEW_HOLDER_TYPE_PREMIUM = R.layout.item_property_premium
        private const val VIEW_HOLDER_TYPE_REGULAR = R.layout.item_property
    }
}

interface OnPropertyClickListener {

    fun onPropertyClicked(property: PropertyObject)
}