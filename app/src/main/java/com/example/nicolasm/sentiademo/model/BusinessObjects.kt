package com.example.nicolasm.sentiademo.model

import android.os.Parcelable
import com.example.nicolasm.sentiademo.endsWithAny
import com.example.wscore.responses.OwnerApiObject
import com.example.wscore.responses.OwnerImageListApiObject
import com.example.wscore.responses.PropertyApiObject
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PropertyObject(val id: Long,
                          val title: String,
                          val imageUrlList: List<String>,
                          val bedrooms: Int,
                          val bathrooms: Int,
                          val carSpots: Int,
                          val description: String,
                          val displayPrice: String,
                          val currency: String,
                          val owner: OwnerObject,
                          val address: String,
                          val address2: String?,
                          val suburb: String,
                          val state: String,
                          val isPremium: Boolean) : Parcelable {

    constructor(apiObject: PropertyApiObject) :
            this(id = apiObject.Id,
                    title = apiObject.Location.Suburb + ", " + apiObject.Location.State,
                    imageUrlList = handleUrlList(apiObject.ImageUrls),
                    bedrooms = apiObject.Bedrooms,
                    bathrooms = apiObject.Bathrooms,
                    carSpots = if (apiObject.Carspaces == null) {
                        0
                    } else {
                        apiObject.Carspaces!!
                    },
                    description = apiObject.Description.orEmpty(),
                    displayPrice = apiObject.DisplayPrice,
                    currency = apiObject.Currency,
                    owner = OwnerObject(apiObject.owner),
                    // in the api result, the suburb is written in the address. We already have it in Location.Suburb
                    address = apiObject.Location.Address.substringBeforeLast(","),
                    address2 = apiObject.Location.Address2?.substringBeforeLast(","),
                    suburb = apiObject.Location.Suburb,
                    state = apiObject.Location.State,
                    isPremium = apiObject.is_premium != null && apiObject.is_premium == 1)

}

/**
 * This method filters the urls to keep only the ones containing a picture.
 * It also sends an empty List if the input from the server is null.
 *
 * @param imageUrlList the list of Urls sent by the server, unverified. Can be null, empty, or contain urls without any pictures
 * @return a List of image files or an empty list if the input from the server is empty, null, or contains no image
 */
private fun handleUrlList(imageUrlList: List<String>?): List<String> {
    val output = imageUrlList?.filter { it.endsWithAny(".jpg", ".png") }
    return output ?: arrayListOf<String>()
}


@Parcelize
data class OwnerObject(val firstName: String,
                       val lastName: String,
                       val dob: String,
                       val picUrl: String) : Parcelable {

    constructor(apiObject: OwnerApiObject) :
            this(apiObject.name,
                    apiObject.lastName,
                    apiObject.dob,
                    handleOwnerPicture(apiObject.image)
            )

}

/**
 * IN OUR EXAMPLE at least, we just need one picture from the owner, and the smallest one will be enough
 * If the smallest one is available, we'll use it. If not, we try the medium, then the big.
 * If non is sent by the api, we use an empty string rather than null, as it will prevent Picasso from crashing
 *
 * @param image the misnamed OwnerImageListApiObject containing a list of image urls. Or none. Or null
 */
private fun handleOwnerPicture(image: OwnerImageListApiObject?): String {
    return when {
        image?.small != null && image.small.url.endsWithAny(".jpg", ".png") -> image.small.url
        image?.medium != null && image.medium.url.endsWithAny(".jpg", ".png") -> image.medium.url
        image?.big != null && image.big.url.endsWithAny(".jpg", ".png") -> image.big.url
        else -> ""
    }
}

/**
 * Used in the [PropertyObjectViewModel] to postValue either the data or an error
 */
data class PropertyObjectWrapper(val properties: List<PropertyObject>?,
                                 val throwable: Throwable?)