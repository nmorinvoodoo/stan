package com.example.nicolasm.sentiademo.ui.propertydetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.nicolasm.sentiademo.R
import com.example.nicolasm.sentiademo.model.PropertyObject
import com.example.nicolasm.sentiademo.ui.propertylist.PropertyListActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_property_detail.*

/**
 * An activity representing a single property detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [PropertyListActivity].
 */
class PropertyDetailsActivity : AppCompatActivity() {

    private lateinit var property: PropertyObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail)
        setSupportActionBar(detail_toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Messages to owners not implemented yet", Snackbar.LENGTH_LONG).show()
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            property = intent.getParcelableExtra(PropertyDetailsFragment.ARG_PROPERTY)
            displayFragment()
            displayImage()
        }
    }

    private fun displayImage() {
        Picasso.get().load(property.imageUrlList[0])
                .error(R.drawable.error_house_img)
                .into(propertyDetailImageView)
    }

    private fun displayFragment() {
        val fragment = PropertyDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PropertyDetailsFragment.ARG_PROPERTY,
                        property)
            }
        }

        supportFragmentManager.beginTransaction()
                .add(R.id.property_detail_container, fragment)
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    onBackPressed()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    companion object {

        fun startActivity(context: Context, property: PropertyObject) {
            val intent = Intent(context, PropertyDetailsActivity::class.java).apply {
                putExtra(PropertyDetailsFragment.ARG_PROPERTY, property)
            }
            context.startActivity(intent)
        }
    }
}
