package com.example.nicolasm.sentiademo.di

import com.example.nicolasm.sentiademo.model.PropertyObjectViewModel
import com.example.wscore.di.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface AppComponent {

    fun inject(target: PropertyObjectViewModel)

}