package com.example.nicolasm.sentiademo.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.nicolasm.sentiademo.SentiaApp
import com.example.wscore.managers.HttpManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PropertyObjectViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var httpManager: HttpManager

    var properties: MutableLiveData<PropertyObjectWrapper> = MutableLiveData()

    init {
        (application as SentiaApp).appComponent?.inject(this)
        fetchData()
    }

    /**
     * Fetch the data from the server
     * Then the map{} transforms the Response<PropertiesResponse> to a List<PropertyListObject> that we can use directly
     * When this is done, we post the value so that the activity can update the UI.
     */
    fun fetchData() {
        httpManager.getPropertyList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { propertyResponse ->
                    val propertyList = ArrayList<PropertyObject>()
                    if (propertyResponse.body() == null) {
                        return@map propertyList
                    }
                    for (propertyApiObject in propertyResponse.body()!!.data.listings) {
                        propertyList.add(PropertyObject(propertyApiObject))
                    }
                    propertyList
                }
                .subscribe({ propertyList ->
                    properties.postValue(PropertyObjectWrapper(propertyList, null))
                }, { throwable: Throwable ->
                    properties.postValue(PropertyObjectWrapper(null, throwable))
                })
    }

    companion object {
        const val TAG = "PropertyObjectViewModel"
    }
}