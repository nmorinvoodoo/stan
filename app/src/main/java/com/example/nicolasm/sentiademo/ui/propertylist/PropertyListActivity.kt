package com.example.nicolasm.sentiademo.ui.propertylist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.nicolasm.sentiademo.ui.propertydetails.PropertyDetailsActivity
import com.example.nicolasm.sentiademo.ui.propertydetails.PropertyDetailsFragment
import com.example.nicolasm.sentiademo.R
import com.example.nicolasm.sentiademo.model.PropertyObject
import com.example.nicolasm.sentiademo.model.PropertyObjectViewModel
import com.example.nicolasm.sentiademo.model.PropertyObjectWrapper
import com.example.nicolasm.sentiademo.ui.propertydetails.IPropertyContainer
import kotlinx.android.synthetic.main.activity_property_list.*
import kotlinx.android.synthetic.main.property_list.*

/**
 * An activity representing a list of Properties.
 * This activity has different presentations for handset and tablet-size devices.
 *      - On handsets, the activity presents a list of items, which when touched, lead to a [PropertyDetailActivity]
 *      representing the property details (just its id, for this demo).
 *      -  On tablets, the activity presents the list of items and item details side-by-side using two vertical panes.
 */
class PropertyListActivity : AppCompatActivity(), OnPropertyClickListener {

    private var isOnTab: Boolean = false

    private var propertyListViewModel: PropertyObjectViewModel? = null

    private var adapter = PropertyAdapter(this)

    //// Android sdk methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_list)

        if (property_detail_container != null) {
            // If this view is present, then the activity is be in two-pane mode.
            isOnTab = true
        }

        setupToolbar()
        setupRecyclerView()
        subscribeToPropertyList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.property_list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_Refresh -> {
                refreshContent()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //// Data fetching/subscribing methods
    private fun subscribeToPropertyList() {
        val propertiesObserver = Observer<PropertyObjectWrapper> { propertiesWrapper: PropertyObjectWrapper? ->
            displayResponse(propertiesWrapper)
        }
        propertyListViewModel = ViewModelProviders.of(this).get(PropertyObjectViewModel::class.java)
        propertyListViewModel?.properties?.observe(this, propertiesObserver)
    }

    private fun refreshContent() {
        propertyListViewModel?.fetchData()
        listActivityProgressbar.visibility = View.VISIBLE
        listActivityRecyclerView.visibility = View.GONE
        listActivityErrorTV.visibility = View.GONE
    }

    //// UI methods
    private fun setupToolbar() {
        setSupportActionBar(listActivityToolbar)
    }

    private fun setupRecyclerView() {
        listActivityRecyclerView.adapter = adapter
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)
        listActivityRecyclerView.addItemDecoration(divider)
    }

    /**
     * Called after the data has been updated.
     * At this point, the content can be either a list of properties or an error.
     */
    private fun displayResponse(propertiesWrapper: PropertyObjectWrapper?) {
        if (propertiesWrapper?.throwable != null) {
            displayError(propertiesWrapper.throwable)
        } else {
            displayProperties(propertiesWrapper?.properties)
        }
    }

    /**
     * Displays the list of properties. Is called when no error has occurred
     */
    private fun displayProperties(properties: List<PropertyObject>?) {
        listActivityProgressbar.visibility = View.GONE
        if (properties == null) {
            displayError(Throwable(getString(R.string.unknown_error_refresh)))
            listActivityProgressbar.visibility = View.VISIBLE
            return
        }
        listActivityRecyclerView.visibility = View.VISIBLE
        adapter.updateContent(properties)
    }

    private fun displayError(throwable: Throwable) {
        if (TextUtils.isEmpty(throwable.message)) {
            listActivityErrorTV.text = getString(R.string.unknown_error_refresh)
        } else {
            listActivityErrorTV.text = getString(R.string.error_refresh, throwable.message)
        }
        listActivityErrorTV.visibility = View.VISIBLE
        listActivityProgressbar.visibility = View.GONE
        if (!listActivityErrorTV.hasOnClickListeners()) {
            listActivityErrorTV.setOnClickListener { refreshContent() }
        }
    }

    /**
     * Called from [AbstractPropertyViewHolder] when the user clicks on a property item
     * If the activity is in two pane mode (tablets)
     *      if the details fragment exists, we update its content, else we create a new one with the property clicked
     * else we create and display a [PropertyDetailsActivity]
     */
    override fun onPropertyClicked(property: PropertyObject) {
        if (isOnTab) {
            val currentFragment = supportFragmentManager.findFragmentByTag(PropertyDetailsFragment.TAG) as IPropertyContainer?
            if (currentFragment == null) {
                displayNewFragment(property)
            } else {
                currentFragment.updateProperty(property)
            }
        } else {
            PropertyDetailsActivity.startActivity(this, property)
        }
    }

    private fun displayNewFragment(property: PropertyObject) {
        val fragment = PropertyDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(PropertyDetailsFragment.ARG_PROPERTY, property)
            }
        }
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.property_detail_container, fragment, PropertyDetailsFragment.TAG)
                .commit()
    }
}
