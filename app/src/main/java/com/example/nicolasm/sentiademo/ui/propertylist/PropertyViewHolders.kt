package com.example.nicolasm.sentiademo.ui.propertylist

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.nicolasm.sentiademo.R
import com.example.nicolasm.sentiademo.model.PropertyObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.include_rooms.view.*
import kotlinx.android.synthetic.main.item_property.view.*
import kotlinx.android.synthetic.main.item_property_premium.view.itemPropertyDescription
import kotlinx.android.synthetic.main.item_property_premium.view.itemPropertyAddress2
import kotlinx.android.synthetic.main.item_property_premium.view.itemPropertyOwnerPicture
import kotlinx.android.synthetic.main.item_property_premium.view.itemPropertyOwnerName


abstract class AbstractPropertyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    init {
        itemView.setOnClickListener {
            listener?.onPropertyClicked(property)
        }
    }

    protected lateinit var property: PropertyObject

    var listener: OnPropertyClickListener? = null

    open fun bind(prop: PropertyObject) {
        property = prop

        itemView.apply {
            // texts
            itemPropertyTitle.text = property.title
            itemPropertyAddress.text = property.address
            itemPropertyPrice.text = "${property.displayPrice} ${property.currency}"

            //image
            Picasso.get().load(property.imageUrlList[0])
                    .error(R.drawable.error_house_img)
                    .into(itemPropertyImageView)
            itemPropertyBedrooms.text = property.bedrooms.toString()
            itemPropertyBathrooms.text = property.bathrooms.toString()

            // rooms logo (# of bedrooms, bathrooms, car spots)
            if (property.carSpots == 0) {
                itemPropertyCarSpotsTV.visibility = View.GONE
                itemPropertyIconCar.visibility = View.GONE
            } else {
                itemPropertyCarSpotsTV.text = property.carSpots.toString()
                itemPropertyCarSpotsTV.visibility = View.VISIBLE
                itemPropertyIconCar.visibility = View.VISIBLE
            }
        }
    }

}

class PropertyViewHolder(itemView: View) : AbstractPropertyViewHolder(itemView) {
    // todo find something else to differentiate them
}

class PremiumPropertyViewHolder(itemView: View) : AbstractPropertyViewHolder(itemView) {

    override fun bind(prop: PropertyObject) {
        super.bind(prop)
        itemView.apply {
            itemView.itemPropertyDescription.text = property.description
            if (property.address2 != null) {
                itemView.itemPropertyAddress2.text = property.address2
                itemView.itemPropertyAddress2.visibility = View.VISIBLE
            } else {
                itemView.itemPropertyAddress2.visibility = View.GONE
            }
            itemView.itemPropertyOwnerName.text = "${property.owner.firstName} ${property.owner.lastName}"
            Picasso.get().load(property.owner.picUrl).into(itemView.itemPropertyOwnerPicture)
        }

    }
}