package com.example.wscore

/**
 * Returns a string representation of the object. Can be called with a null receiver, in which case
 * it returns the empty string "".
 */
fun Any?.toStringOrEmpty(): String {
    if (this != null) {
        return toString()
    } else {
        return ""
    }
}