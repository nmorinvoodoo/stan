package com.example.wscore.exceptions;

import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * This transformer transforms the retrofit Result to a Response.
 * In case the Result is in error, it specifies the error to be able to handle it correctly.
 * With retrofit, when the result response is successful, whatever the response code is,
 * it will go through the onNext callback (is successful means has a http code).
 * To avoid accepting error http codes, this code is checked and if it is not valid
 *  this transformer will throw a more appropriate exception.
 */
class WSResponseRxTransformer<T> : SingleTransformer<Result<T>, Response<T>> {

    override fun apply(upstream: Single<Result<T>>): SingleSource<Response<T>> =
            upstream.map { result ->
                return@map when {
                    result.isError -> throw getError(result.error()!!)
                    result.response()?.isSuccessful == false -> throw getHttpError(result.response()!!)
                    else -> result.response()
                }
            }

    /**
     * Gets a more precise http error depending on the retrofit result [error] which is too generic.
     */
    private fun getError(error: Throwable): Exception {
        return when (error) {
            is SocketTimeoutException -> HttpNoInternetConnectionException()
            is IOException -> HttpNoInternetConnectionException()
            else -> HttpGeneralErrorException(error)
        }
    }

    /**
     * Gets a more precise http error depending on the [response] code.
     */
    private fun getHttpError(response: Response<T>): Exception {
        return when {
            isInErrorRange(response.code(), HTTP_CLIENT_ERROR_RANGE) -> HttpStatusClientErrorException(response)
            isInErrorRange(response.code(), HTTP_SERVER_ERROR_RANGE) -> HttpStatusServerErrorException(response)
            else -> HttpException(response)
        }
    }

    /**
     * Checks if the http [code] is in the supplied [range].
     * The range lower bound is included and the upper bound is excluded.
     */
    private fun isInErrorRange(code: Int, range: IntArray): Boolean {
        if (range.size != 2)
            throw IllegalArgumentException("Error, range size must be 2")
        return range[0] <= code && code < range[1]
    }

    companion object {
        /**
         * Range for client errors such as bad request (400), unauthorized (401), not found (404)...
         */
        private val HTTP_CLIENT_ERROR_RANGE = intArrayOf(400, 500)

        /**
         * Range for server errors such internal server error (500), bad gateway (502) ...
         */
        private val HTTP_SERVER_ERROR_RANGE = intArrayOf(500, 600)
    }
}