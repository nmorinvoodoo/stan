package com.example.wscore.exceptions

import retrofit2.HttpException
import retrofit2.Response

class HttpGeneralErrorException(throwable: Throwable) : Exception(throwable)

class HttpNoInternetConnectionException : Exception("Cannot connect to the server")

abstract class AbstractHttpStatusException(response: Response<*>) : Exception() {

    private val httpException: HttpException = HttpException(response)

    override val message: String?
        get() = httpException.message()
}

class HttpStatusClientErrorException(response: Response<*>) : AbstractHttpStatusException(response)

class HttpStatusServerErrorException(response: Response<*>) : AbstractHttpStatusException(response)

