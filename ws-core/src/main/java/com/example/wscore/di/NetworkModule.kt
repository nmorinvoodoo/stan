package com.example.wscore.di

import com.example.wscore.managers.HttpManagerImpl
import com.example.wscore.BuildConfig
import com.example.wscore.managers.HttpManager
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        private const val NAME_BASE_URL = "NAME_BASE_URL"
    }

    @Provides
    @Named(NAME_BASE_URL)
    fun provideBaseUrlString() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideHttpManager(@Named(NAME_BASE_URL) baseUrl: String): HttpManager = HttpManagerImpl(baseUrl)
}