package com.example.wscore.api

import com.example.wscore.responses.PropertiesResponse
import io.reactivex.Single
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.GET

interface RequestService {

    @GET("listings")
    fun getPropertyList(): Single<Result<PropertiesResponse>>

}