package com.example.wscore.responses

data class PropertiesResponse(val data: UnusedDataResponseClass)

data class UnusedDataResponseClass(val listings: List<PropertyApiObject>)

data class PropertyApiObject(val Id: Long,
                             val AgencyLogoUrl: String?,
                             val Area: String,
                             val AuctionDate: String,
                             val AvailableFrom: String?,
                             val Bedrooms: Int,
                             val Bathrooms: Int,
                             val Carspaces: Int?,
                             val DateFirstListed: String,
                             val DateUpdated: String,
                             val Description: String?,
                             val DisplayPrice: String,
                             val Currency: String,
                             val Location: LocationApiObject,
                             val owner: OwnerApiObject,
                             val ImageUrls: List<String>?,
                             val is_premium: Int?,
                             val IsPriority: Int?,
                             val Latitude: Float,
                             val Longitude: Float)

data class LocationApiObject(val Address: String,
                             val Address2: String?,
                             val State: String,
                             val Suburb: String)

data class OwnerApiObject(val name: String,
                          val lastName: String,
                          val dob: String,
                          val image: OwnerImageListApiObject?)

data class OwnerImageListApiObject(val big: OwnerImageApiObject,
                                   val small: OwnerImageApiObject,
                                   val medium: OwnerImageApiObject)

data class OwnerImageApiObject(val url: String)