package com.example.wscore.managers

import com.example.wscore.responses.*

import io.reactivex.Single
import retrofit2.Response

interface HttpManager {

    fun getPropertyList(): Single<Response<PropertiesResponse>>

}