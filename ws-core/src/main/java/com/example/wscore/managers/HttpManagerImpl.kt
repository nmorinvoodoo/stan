package com.example.wscore.managers


import com.example.wscore.api.RequestService
import com.example.wscore.responses.PropertiesResponse
import com.example.wscore.BuildConfig
import com.example.wscore.exceptions.WSResponseRxTransformer
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class HttpManagerImpl(baseUrl: String) : HttpManager {

    private var requestService: RequestService

    init {
        val httpClient = OkHttpClient().newBuilder()

        // add the Logging Interceptor
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
            httpClient.addInterceptor(httpLoggingInterceptor)
        }

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        requestService = retrofit.create(RequestService::class.java)
    }


    // API Calls

    override fun getPropertyList(): Single<Response<PropertiesResponse>> {
        return requestService.getPropertyList().compose(WSResponseRxTransformer())
    }
}